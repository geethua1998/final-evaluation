class Pet:
    spe=['dog','cat','horse','hamster']
    def __init__(self, species= None, name=""):
        self.species=species
        self.name=name
    def __str__(self):
        x=len(self.name)
        if self.species.lower() not in self.spe:
            return "Error! Not a valid species"
        if x>0:
            return "Species of "+self.species+" ,named "+self.name
        elif x==0:
            return "Species of "+self.species+" ,unnamed. "
class Dog(Pet):
    def __init__(self,name,species="dog",chases="Cats"):
        self.chases=chases
        self.species = "dog"
        Pet.__init__(self,species, name)
    def __str__(self):
        x = len(self.name)
        if x > 0:
            return "Species of: " + self.species + " ,named " + self.name +" ,chases "+self.chases
        elif x == 0:
            return "Species of: " + self.species + " , unnamed, chases "+self.chases

class Cat(Pet):
    def __init__(self,name,species='cat', hates="Dogs"):
        self.hates=hates
        self.species = "cat"
        Pet.__init__(self,species,name)
    def __str__(self):
        x = len(self.name)
        if x > 0:
            return "Species of: " + self.species + " ,named " + self.name +" ,hates "+self.hates
        elif x == 0:
            return "Species of: " + self.species + " , unnamed, hates "+self.hates

if __name__ == '__main__':
    print("Choose a Pet!")
    s=input().capitalize()
    n=input("Give your pet a name!").capitalize()
    p=Pet(s,n)
    print(p.__str__())
    if s=="Dog":
        ch=input("what does your dog chase after?")
        d=Dog(n,chases=ch)
        print(d.__str__())
    elif s=="Cat":
        ha=input("What does your cat hate?")
        c=Cat(n,hates=ha)
        print(c.__str__())

